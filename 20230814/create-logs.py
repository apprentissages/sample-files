import json
import time

ts = int(time.time_ns() / 1000000)
with open('nginx-log.log') as f:
    rows = [{'timestamp': ts, 'message': line } for line in f]


with open('logs.json', 'w') as wf:
    wf.write(json.dumps(rows))
