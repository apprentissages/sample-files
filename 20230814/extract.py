import boto3
from datetime import datetime, timezone, timedelta

tz = timezone(timedelta(hours=9))
dt_base = datetime.now(tz)
dt_from = datetime(dt_base.year, dt_base.month, dt_base.day - 1, 0, 0, 0, tzinfo=tz)
dt_to = datetime(dt_base.year, dt_base.month, dt_base.day - 1, 23, 59, 59, tzinfo=tz)
client = boto3.client('logs')

response = client.filter_log_events(
    logGroupName='/nginx-log-test',
    startTime=int(dt_from.timestamp() * 1000),
    endTime=int(dt_to.timestamp() * 1000),
)

print(response)
