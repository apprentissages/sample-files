const faker = require('faker');
const moment = require('moment');
const fs = require('fs');

const stream = fs.createWriteStream('./access.log', {
  flags: 'a'
});

function writeToStream(n) {
  for (; n < 1000000; n++) {
    const access = faker.fake(`{{internet.ip}} - - [${timestamp()}] "GET /{{internet.domainWord}}/{{lorem.slug}} HTTP/1.1" 200 {{random.number}} "-" "{{internet.userAgent}}"`);
    if (!stream.write(`${access}\n`)) {
      stream.once('drain', () => writeToStream(n + 1))
      return;
    }
  }
  stream.end();
}

writeToStream(0);

function timestamp () {
  const ts = faker.date.recent(7);
  const str = moment(ts).format('DD/MMM/YYYY:HH:mm:ss ZZ');
  return str;
}
