import os
import json
from slack_sdk import WebClient

TOKEN = 'xoxb-4445745425-5532483777845-fAEglyctw0ajCGgd61SkSTCj'
client = WebClient(token=TOKEN)
response = client.api_call(
    api_method='chat.postMessage',
    json={'channel': '#misc',
          "blocks": [
              {
                  "type": "header",
                  "text": {
                      "type": "plain_text",
                      "text": "New request",
                      "emoji": True
                  }
              },
              {
                  "type": "section",
                  "fields": [
                      {
                          "type": "mrkdwn",
                          "text": "*Type:*\nPaid Time Off"
                      },
                      {
                          "type": "mrkdwn",
                          "text": "*Created by:*\n<example.com|Fred Enriquez>"
                      }
                  ]
              },
              {
                  "type": "section",
                  "fields": [
                      {
                          "type": "mrkdwn",
                          "text": "*When:*\nAug 10 - Aug 13"
                      },
                      {
                          "type": "mrkdwn",
                          "text": "*Type:*\nPaid time off"
                      }
                  ]
              },
              {
                  "type": "section",
                  "fields": [
                      {
                          "type": "mrkdwn",
                          "text": "*Hours:*\n16.0 (2 days)"
                      },
                      {
                          "type": "mrkdwn",
                          "text": "*Remaining balance:*\n32.0 hours (4 days)"
                      }
                  ]
              },
              {
                  "type": "section",
                  "text": {
                      "type": "mrkdwn",
                      "text": "<https://example.com|View request>"
                  }
              }
          ]
          }
)
assert response["ok"]
assert response["message"]["text"] == "Hello world!"
